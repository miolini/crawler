package main

import (
	"bytes"
	"code.google.com/p/go.net/html"
	"flag"
	"fmt"
	"github.com/miolini/logdb"
	"gopkg.in/vmihailenco/msgpack.v2"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"
	"sync/atomic"
	"time"
	"hash/fnv"
	"./diskqueue"
)

var (
	flagBaseUrl        = flag.String("base-url", "", "base url")
	flagConns          = flag.Int("c", 10, "connections")
	flagMaxPages       = flag.Int("max", 1000000, "max pages")
	flagRegexpStop     = flag.String("stop", "", "regexp for stop url")
	flagRegexpPass     = flag.String("pass", "", "regexp for pass url")
	flagDatabase       = flag.String("db", "outputdb", "database")
	flagCleanUri       = flag.Bool("clean-uri", true, "remove query part of uri")
	flagRmPattern      = flag.String("rmp", "", "remove regexp pattern")
)

func main() {
	flag.Parse()
	crawler := NewCrawler(*flagBaseUrl, *flagConns, *flagMaxPages,
		*flagRegexpStop, *flagRegexpPass, *flagRmPattern,
		*flagDatabase, *flagCleanUri)
	go func() {
		http.ListenAndServe("localhost:8080", nil)
	}()
	crawler.Run()
}

type Page struct {
	Url         string `json:"url"`
	Host        string `json:"host,omitempty"`
	Level       int    `json:"level,omitempty"`
	ContentType string `json:"content_type,omitempty"`
	Body        []byte `json:"body,omitempty"`
	Title       string `json:"title,omitempty"`
	StatusCode  int    `json:"status_code"`
	Timestamp   int64  `json:"timestamp"`
	Error       string `json:"error,omitempty"`
}

type Crawler struct {
	baseUrl        string
	conns          int
	maxPages       int
	chanJob        chan Page
	chanResult     chan Page
	chanFilter     chan string
	inFlight       int64
	urlSet         map[uint64]uint8
	urlQueue       *diskqueue.Queue
	urlQueueLen    int64
	counterFetched uint64
	regexpStop     *regexp.Regexp
	regexpPass     *regexp.Regexp
	regexpRm       *regexp.Regexp
	outputDb       *logdb.DB
	isCleanUri     bool
}

func NewCrawler(baseUrl string, conns int, maxPages int, 
	regexpStop, regexpPass, regexpRm string,
	outputDbPath string, isCleanUri bool) (crawler *Crawler) {
	var err error
	crawler = new(Crawler)
	crawler.baseUrl = baseUrl
	crawler.conns = conns
	crawler.maxPages = maxPages
	crawler.chanJob = make(chan Page, 100)
	crawler.chanResult = make(chan Page, 100)
	crawler.chanFilter = make(chan string, 100)
	crawler.urlSet = make(map[uint64]uint8)
	parsedUrl, _ := url.Parse(baseUrl)
	crawler.urlQueue, err = diskqueue.New(parsedUrl.Host+".q", time.Second * 5)
	if err != nil {
		log.Fatalf("open queue err: %s", err)
		return
	}
	crawler.isCleanUri = true
	if regexpStop != "" {
		crawler.regexpStop = regexp.MustCompile(regexpStop)
	}
	if regexpPass != "" {
		crawler.regexpPass = regexp.MustCompile(regexpPass)
	}
	if regexpRm != "" {
		crawler.regexpRm = regexp.MustCompile(regexpRm)
	}
	crawler.outputDb, err = logdb.Open(outputDbPath, &logdb.Config{WriteBufferSize: 1024 * 512, SplitSize: 1024 * 1024 * 1024})
	if err != nil {
		log.Fatalf("open database err: %s", err)
	}
	return
}

func (crawler *Crawler) Run() {
	waitGroup := sync.WaitGroup{}
	waitGroup.Add(1)
	go crawler.workerResultSaver(&waitGroup)
	waitGroup.Add(1)
	go crawler.workerFilterProcessedPages(&waitGroup)
	waitGroup.Add(crawler.conns)
	for i := 0; i < crawler.conns; i++ {
		go crawler.workerFetch(&waitGroup)
	}
	go crawler.AddUrl(crawler.baseUrl, true)
	ts := time.Now()
	for {
		if time.Since(ts) > time.Second * 30 {
			crawler.exit()
		}
		pageUrlBytes, ok, err := crawler.urlQueue.Dequeue()
		if err != nil {
			crawler.exit()
		}
		if !ok {
			time.Sleep(time.Millisecond * 50)
			continue
		}
		ts = time.Now()
		atomic.AddInt64(&crawler.urlQueueLen, -1)
		crawler.chanJob <- Page{Url: string(pageUrlBytes)}
	}
}

func (crawler *Crawler) exit() {
	crawler.outputDb.Close()
	crawler.urlQueue.Close()
	os.Exit(0)
}

func (crawler *Crawler) workerResultSaver(waitGroup *sync.WaitGroup) {
	defer func() {
		waitGroup.Done()
		crawler.outputDb.Close()
		if r := recover(); r != nil {
			log.Printf("result savar panic: %s", r)
		}
	}()
	log.Printf("new worker result saver")
	for {
		select {
		case page, ok := <-crawler.chanResult:
			if !ok {
				log.Printf("crawler exit")
				return
			}
			counterFetched := atomic.AddUint64(&crawler.counterFetched, 1)
			queueLen := atomic.LoadInt64(&crawler.urlQueueLen)
			log.Printf("fetched %d/%d/%d %d/%d %s %s", counterFetched, queueLen, int64(counterFetched)+queueLen, page.StatusCode, len(page.Body), page.ContentType, page.Url)
			data, err := msgpack.Marshal(page)

			if err != nil {
				log.Fatalf("can't marshal page: %s", err)
			}
			err = crawler.outputDb.Write(data)
			if err != nil {
				crawler.outputDb.Close()
				log.Fatalf("can't save page: %s", err)
			}

		case <-time.After(30 * time.Second):
			crawler.outputDb.Close()
			os.Exit(0)
		}
	}
}

var regexp0A = regexp.MustCompile("(%0A)")

func (crawler *Crawler) AddUrl(pageUrl string, force bool) {
	pageUrl = regexp0A.ReplaceAllString(pageUrl, "%0A")
	if strings.Contains(pageUrl, "%0A") {
		pageUrls := strings.Split(pageUrl, "%0A")
		for _, pageUrl = range pageUrls {
			crawler.AddUrl(pageUrl, force)
		}
		return
	}
	if crawler.regexpRm != nil {
		pageUrl = crawler.regexpRm.ReplaceAllString(pageUrl, "")
	}
	pageUrlBytes := []byte(pageUrl)
	urlHash := fnv64Hash(pageUrl)
	if !force && crawler.urlSet[urlHash] == 1 {
		return
	}
	if !force && crawler.regexpStop != nil && crawler.regexpStop.Match(pageUrlBytes) {
		return
	}
	if !force && crawler.regexpPass != nil && !crawler.regexpPass.Match(pageUrlBytes) {
		return
	}
	crawler.urlSet[urlHash] = 1
	crawler.urlQueue.Enqueue([]byte(pageUrl))
	atomic.AddInt64(&crawler.urlQueueLen, 1)
}

func fnv64Hash(s string) uint64 {
	h := fnv.New64()
	h.Write([]byte(s))
	return h.Sum64()
}

func (crawler *Crawler) workerFetch(waitGroup *sync.WaitGroup) {
	time.Sleep(time.Duration(rand.Int()%15) * time.Second)
	defer func() {
		waitGroup.Done()
		return
		if r := recover(); r != nil {
			log.Fatalf("fetch panic: %s", r)
			panic(r)
		}
	}()
	transport := http.Transport{}
	transport.DisableCompression = false
	transport.DisableKeepAlives = false
	transport.MaxIdleConnsPerHost = 100
	transport.ResponseHeaderTimeout = 30 * time.Second
	httpClient := http.Client{Transport: &transport}
	for page := range crawler.chanJob {
		crawler.fetchPage(&page, &httpClient)
		if page.Error != "" {
			log.Printf("fetch err: %s", page.Error)
			continue
		}
		if !strings.HasPrefix(page.ContentType, "text/html") {
			page.Error = fmt.Sprintf("bad content-type: %s", page.ContentType)
			return
		} else {
			doc, err := html.Parse(bytes.NewReader(page.Body))
			if err != nil {
				page.Error = fmt.Sprintf("html parse error: %s", err)
			} else {
				crawler.findLinks(&page, doc)
				crawler.findTitle(&page, doc)
			}
		}
		crawler.chanResult <- page
	}
}

func (crawler *Crawler) findTitle(page *Page, doc *html.Node) {
	var f func(*html.Node)
	f = func (n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "title" {
			if n.FirstChild != nil {
				page.Title = n.FirstChild.Data
			}
			return
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
}

func (crawler *Crawler) findLinks(page *Page, doc *html.Node) {
	parentUrl, _ := url.Parse(page.Url)
	page.Host = parentUrl.Host
	commonHost := page.Host
	if strings.HasPrefix(commonHost, "www.") {
		commonHost = commonHost[4:]
	}
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "a" {
			href := getNodeAttr(n, "href")
			hrefUrl, err := url.Parse(href)
			if err != nil {
				log.Printf("can't parse url: %s", href)
				return
			}
			if crawler.isCleanUri {
				hrefUrl.RawQuery = ""
			}
			hrefUrl.Fragment = ""
			hrefUrl = parentUrl.ResolveReference(hrefUrl)
			/*if hrefUrl.Scheme != parentUrl.Scheme {
				log.Printf("bad scheme url: %s", hrefUrl.String())
				return
			}
			*/
//			log.Printf("try add url %s", hrefUrl)
			if !strings.HasSuffix(hrefUrl.Host, commonHost) {
				//log.Printf("different host: %s", hrefUrl.String())
				return
			}
			if hasExts(hrefUrl.Path, ".jpg", ".jpeg", ".png", ".gif", ".ico", ".js", ".css", ".bmp", ".pdf") {
				//log.Printf("bad ext: %s", hrefUrl.Path)
				return
			}
			crawler.chanFilter <- hrefUrl.String()
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
}

func hasExts(uri string, exts...string) bool {
	uri = strings.ToLower(uri)
	for _, ext := range exts {
		if strings.HasSuffix(uri, ext) {
			return true
		}
	}
	return false
}

func (crawler *Crawler) workerFilterProcessedPages(waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	for pageUrl := range crawler.chanFilter {
		crawler.AddUrl(pageUrl, false)
	}
	return
}

func getNodeAttr(node *html.Node, attrName string) string {
	for _, attr := range node.Attr {
		if attr.Key == attrName {
			return attr.Val
		}
	}
	return ""
}

func (crawler *Crawler) fetchPage(page *Page, httpClient *http.Client) {
	req, err := http.NewRequest("GET", page.Url, nil)
	if err != nil {
		page.Error = err.Error()
		return
	}
	req.Header.Set("User-Agent", "Mozilla 3.27 (cleared Gecko/Windows9)")
	req.Header.Set("Connection", "Keep-Alive")
	resp, err := httpClient.Do(req)
	if err != nil {
		page.Error = fmt.Sprintf("error crawler can't get page %s: %s", page.Url, err.Error())
		return
	}
	defer resp.Body.Close()
	page.StatusCode = resp.StatusCode
	page.ContentType = resp.Header.Get("Content-Type")
	if !strings.HasPrefix(page.ContentType, "text/html") {
		page.Error = fmt.Sprintf("bad content-type: %s", page.ContentType)
		return
	}
	page.Body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		page.Error = fmt.Sprintf("error crawler can't read page %s: %s", page.Url, err.Error())
		return
	}
}
