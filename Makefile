SOURCE=crawler.go
TARGET=crawler

build:
	go build -o $(TARGET) $(SOURCE)

clean:
	rm -rf $(TARGET)

install: build
	cp $(TARGET) /usr/local/bin