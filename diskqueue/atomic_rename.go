// +build !windows

package diskqueue

import (
	"os"
)

func atomic_rename(source_file, target_file string) error {
	return os.Rename(source_file, target_file)
}