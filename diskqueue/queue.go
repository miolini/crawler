package diskqueue

import "time"
import "os"
import "bufio"
import "encoding/binary"
import "sync/atomic"
import "sync"

type Queue struct {
	ReadChan chan []byte
	WriteChan chan []byte
	writerFile *os.File
	writer *bufio.Writer
	writerMutex sync.Mutex
	writerPos int64
	readerFile *os.File
	reader *bufio.Reader
	readerMutex sync.Mutex
	length int64
	syncTicker *time.Ticker
	
}

func New(path string, syncTimeout time.Duration) (q *Queue, err error) {
	q = new(Queue)
	q.syncTicker = time.NewTicker(syncTimeout)
	q.ReadChan = make(chan []byte)
	q.WriteChan = make(chan []byte)
	if q.writerFile, err = os.Create(path); err!=nil {
		return
	}
	q.writer = bufio.NewWriterSize(q.writerFile, 1024 * 256)
	if q.readerFile, err = os.Open(path); err != nil {
		q.writerFile.Close()
		return
	}
	q.reader = bufio.NewReaderSize(q.readerFile, 1024 * 256)
	go func() {
		for {
			time.Sleep(syncTimeout)
			q.writerMutex.Lock()
			q.writer.Flush()
			q.writerFile.Sync()
			q.writerMutex.Unlock()
		}
	}()
	return
}

func (q *Queue) Enqueue(data []byte) (err error) {
	q.writerMutex.Lock()
	defer q.writerMutex.Unlock()
	len := len(data)
	binary.Write(q.writer, binary.LittleEndian, &len)
	q.writer.Write(data)
	q.writer.Flush()
	atomic.AddInt64(&q.length, 1)
	return
}

func (q *Queue) Dequeue() (data []byte, ok bool, err error) {
	q.readerMutex.Lock()
	defer q.readerMutex.Unlock()
	atomic.AddInt64(&q.length, -1)
	return
}

func (q *Queue) Close() {
	q.writerMutex.Lock()
	q.readerMutex.Lock()
	defer q.writerMutex.Unlock()
	defer q.readerMutex.Unlock()
	q.writer.Flush()
	q.writerFile.Close()
	q.readerFile.Close()
}